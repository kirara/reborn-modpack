// --- Created by Zulfurlubak for KR ---
// --- Saddle
recipes.addShaped(<minecraft:saddle>, [
[<minecraft:leather>, <minecraft:leather>, <minecraft:leather>],
[<minecraft:leather>, <minecraft:carpet:*>, <minecraft:leather>],
[<ore:ringAnyIron>, <minecraft:string>, <ore:ringAnyIron>]]);

// --- Variables ---


// --- Removing Recipes ---


// --- Iron Chest
recipes.remove(<IronChest:BlockIronChest>);
// --- Gold Chest
recipes.remove(<IronChest:BlockIronChest:1>);
// --- Diamond Chest
recipes.remove(<IronChest:BlockIronChest:2>);
// --- Copper Chest
recipes.remove(<IronChest:BlockIronChest:3>);
// --- Silver Chest
recipes.remove(<IronChest:BlockIronChest:4>);
// --- Obsidian Chest
recipes.remove(<IronChest:BlockIronChest:6>);
// --- Iron to Gold Chest Upgrade
recipes.remove(<IronChest:ironGoldUpgrade>);
// --- Gold to Diamond Chest Upgrade
recipes.remove(<IronChest:goldDiamondUpgrade>);
// --- Copper to Silver Chest Upgrade
recipes.remove(<IronChest:copperSilverUpgrade>);
// --- Silver to Gold Chest Upgrade
recipes.remove(<IronChest:silverGoldUpgrade>);
// --- Copper to Iron Chest Upgrade
recipes.remove(<IronChest:copperIronUpgrade>);
// --- Diamond to Crystal Chest Upgrade
recipes.remove(<IronChest:diamondCrystalUpgrade>);
// --- Wood to Iron Chest Upgrade
recipes.remove(<IronChest:woodIronUpgrade>);
// --- Wood to Copper Chest Upgrade
recipes.remove(<IronChest:woodCopperUpgrade>);
// --- Diamond to Obsidian Chest Upgrade
recipes.remove(<IronChest:diamondObsidianUpgrade>);


// --- Adding Back Recipes ---


// --- Iron Chest
recipes.addShaped(<IronChest:BlockIronChest>, [
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <minecraft:chest>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>]]);
//-
recipes.addShaped(<IronChest:BlockIronChest>, [
[<ore:plateAnyIron>, <ore:blockGlass>, <ore:plateAnyIron>],
[<ore:blockGlass>, <IronChest:BlockIronChest:3>, <ore:blockGlass>],
[<ore:plateAnyIron>, <ore:blockGlass>, <ore:plateAnyIron>]]);

// --- Gold Chest
recipes.addShaped(<IronChest:BlockIronChest:1>, [
[<ore:plateGold>, <ore:plateGold>, <ore:plateGold>],
[<ore:plateGold>, <IronChest:BlockIronChest>, <ore:plateGold>],
[<ore:plateGold>, <ore:plateGold>, <ore:plateGold>]]);
//-
recipes.addShaped(<IronChest:BlockIronChest:1>, [
[<ore:plateGold>, <ore:blockGlass>, <ore:plateGold>],
[<ore:blockGlass>, <IronChest:BlockIronChest:4>, <ore:blockGlass>],
[<ore:plateGold>, <ore:blockGlass>, <ore:plateGold>]]);

// --- Diamond Chest
recipes.addShaped(<IronChest:BlockIronChest:2>, [
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
[<ore:plateDiamond>, <IronChest:BlockIronChest:1>, <ore:plateDiamond>],
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>]]);
//-
recipes.addShaped(<IronChest:BlockIronChest:2>, [
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
[<ore:blockGlass>, <IronChest:BlockIronChest:4>, <ore:blockGlass>],
[<ore:plateDiamond>, <ore:plateDiamond>, <ore:plateDiamond>]]);

// --- Copper Chest
recipes.addShaped(<IronChest:BlockIronChest:3>, [
[<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>],
[<ore:plateCopper>, <minecraft:chest>, <ore:plateCopper>],
[<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>]]);

// --- Silver Chest
recipes.addShaped(<IronChest:BlockIronChest:4>, [
[<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>],
[<ore:plateSilver>, <IronChest:BlockIronChest:3>, <ore:plateSilver>],
[<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>]]);
//-
recipes.addShaped(<IronChest:BlockIronChest:4>, [
[<ore:plateSilver>, <ore:blockGlass>, <ore:plateSilver>],
[<ore:blockGlass>, <IronChest:BlockIronChest>, <ore:blockGlass>],
[<ore:plateSilver>, <ore:blockGlass>, <ore:plateSilver>]]);

// --- Obsidian Chest
recipes.addShaped(<IronChest:BlockIronChest:6>, [
[<ore:plateObsidian>, <ore:plateObsidian>, <ore:plateObsidian>],
[<ore:plateObsidian>, <IronChest:BlockIronChest:2>, <ore:plateObsidian>],
[<ore:plateObsidian>, <ore:plateObsidian>, <ore:plateObsidian>]]);

// --- Iron to Gold Chest Upgrade
recipes.addShaped(<IronChest:ironGoldUpgrade>, [
[<ore:plateGold>, <ore:plateGold>, <ore:plateGold>],
[<ore:plateGold>, <ore:plateAnyIron>, <ore:plateGold>],
[<ore:plateGold>, <ore:plateGold>, <ore:plateGold>]]);

// --- Gold to Diamond Chest Upgrade
recipes.addShaped(<IronChest:goldDiamondUpgrade>, [
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
[<ore:plateDiamond>, <ore:plateGold>, <ore:plateDiamond>],
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>]]);

// --- Copper to Silver Chest Upgrade
recipes.addShaped(<IronChest:copperSilverUpgrade>, [
[<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>],
[<ore:plateSilver>, <ore:plateCopper>, <ore:plateSilver>],
[<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>]]);

// --- Silver to Gold Chest Upgrade
recipes.addShaped(<IronChest:silverGoldUpgrade>, [
[<ore:plateGold>, <ore:blockGlass>, <ore:plateGold>],
[<ore:blockGlass>, <ore:plateSilver>, <ore:blockGlass>],
[<ore:plateGold>, <ore:blockGlass>, <ore:plateGold>]]);

// --- Copper to Iron Chest Upgrade
recipes.addShaped(<IronChest:copperIronUpgrade>, [
[<ore:plateAnyIron>, <ore:blockGlass>, <ore:plateAnyIron>],
[<ore:blockGlass>, <ore:plateCopper>, <ore:blockGlass>],
[<ore:plateAnyIron>, <ore:blockGlass>, <ore:plateAnyIron>]]);

// --- Diamond to Crystal Chest Upgrade
recipes.addShaped(<IronChest:diamondCrystalUpgrade>, [
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
[<ore:blockGlass>, <ore:plateDiamond>, <ore:blockGlass>],
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>]]);

// --- Wood to Iron Chest Upgrade
recipes.addShaped(<IronChest:woodIronUpgrade>, [
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <ore:plankWood>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>]]);

// --- Wood to Copper Chest Upgrade
recipes.addShaped(<IronChest:woodCopperUpgrade>, [
[<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>],
[<ore:plateCopper>, <ore:plankWood>, <ore:plateCopper>],
[<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>]]);

// --- Diamond to Obsidian Chest Upgrade
recipes.addShaped(<IronChest:diamondObsidianUpgrade>, [
[<ore:plateObsidian>, <ore:plateObsidian>, <ore:plateObsidian>],
[<ore:plateObsidian>, <ore:plateDiamond>, <ore:plateObsidian>],
[<ore:plateObsidian>, <ore:plateObsidian>, <ore:plateObsidian>]]);
