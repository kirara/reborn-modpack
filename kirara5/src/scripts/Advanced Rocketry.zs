//***************************
// Kirara5tweak - Advanced Rocketry
//  created by Zulfurlubak
//
// Changelog:
//   v0.1: 
//     - Initial release
//     - Disabled AR processing machines, recipes moved to their respective GT counterparts
//     - "Gregified" a lot of AR recipes
//     - Some stuff like solar panels and automated mining rockets are disabled until later
// 
//***************************

//*******************
// Importing mods
//*******************


import mods.nei.NEI;
import mods.ic2.Compressor;
import mods.gregtech.Centrifuge;
import mods.gregtech.FluidExtractor;
import mods.gregtech.BlastFurnace;
import mods.gregtech.Extruder;
import mods.gregtech.ImplosionCompressor;
import mods.gregtech.Assembler;
import mods.gregtech.Autoclave;
import mods.gregtech.Mixer;


//*******************
// Item declarations
//*******************


//**************************
// Removing default recipes
//**************************

// --- Small Plate Presser
recipes.remove(<advancedRocketry:tile.blockHandPress>);
// --- Lathe
recipes.remove (<advancedRocketry:tile.lathe>);
// --- Rolling Machine
recipes.remove (<advancedRocketry:tile.rollingMachine>);
// --- Cutting Machine
recipes.remove (<advancedRocketry:cuttingMachine>);
// --- Titanium Aluminide Block
recipes.remove (<advancedRocketry:advancedRocketrymetal0>);
// --- Titanium Iridium Alloy Block
recipes.remove (<advancedRocketry:advancedRocketrymetal0:1>);
// --- Drill
recipes.remove (<advancedRocketry:tile.drill>);
// --- Electrolyser
recipes.remove (<advancedRocketry:tile.electrolyser>);
// --- Electric Arc Furnace
recipes.remove (<advancedRocketry:tile.electricArcFurnace>);
// --- Precision Assembler
recipes.remove (<advancedRocketry:precisionassemblingmachine>);
// --- HeatProof Brick
recipes.remove (<advancedRocketry:utilBlock>);
// --- Chemical Reactor
recipes.remove (<advancedRocketry:tile.chemreactor>);
// --- Crystallizer
recipes.remove (<advancedRocketry:crystallizer>);
// --- Aluminum Block
recipes.remove (<libVulpes:libVulpesmetal0:9>);
// --- Smelting Titanium Aluminide
furnace.remove(<*>, <advancedRocketry:advancedRocketryproductdust>);
// --- Smelting Titanium Iridium Alloy
furnace.remove(<*>, <advancedRocketry:advancedRocketryproductdust:1>);
// --- Titanium Aluminide Rod
recipes.removeShaped (<advancedRocketry:advancedRocketryproductrod>);
// --- Titanium Iridium Alloy Rod
recipes.removeShaped (<advancedRocketry:advancedRocketryproductrod:1>);
// --- Launch Pad
recipes.removeShapeless (<advancedRocketry:launchpad>);
// --- Liquid Fueled Engine
recipes.removeShaped (<advancedRocketry:rocketmotor>);
// --- Advanced Liquid Fueled Engine
recipes.removeShaped (<advancedRocketry:tile.advRocket>);
// --- Liquid Fuel Tank
recipes.removeShaped (<advancedRocketry:fuelTank>);
// --- Seat
recipes.removeShaped (<advancedRocketry:seat>);
// --- Jet Pack
recipes.removeShaped (<advancedRocketry:item.jetPack>);
// --- Machine Structure
recipes.removeShaped (<libVulpes:blockStructureBlock>);
// --- Advanced Machine Structure
recipes.removeShaped (<libVulpes:tile.advStructureMachine>);
// --- User Interface
recipes.removeShaped (<advancedRocketry:miscpart>);
// --- Quartz Crucible
recipes.removeShaped (<advancedRocketry:iquartzcrucible>);
// --- Rocket Assembling Machine
recipes.removeShaped (<advancedRocketry:rocketBuilder>);
// --- Observatory
recipes.remove (<advancedRocketry:observatory>);
// --- Fueling Station
recipes.remove (<advancedRocketry:fuelingStation>);
// --- Satellite Terminal
recipes.remove (<advancedRocketry:tile.satelliteMonitor>);
// --- Astrobody Data Processor
recipes.remove (<advancedRocketry:tile.planetanalyser>);
// --- Gas Charge Pad
recipes.remove (<advancedRocketry:tile.oxygenCharger>);
// --- Atmosphere Detector
recipes.remove (<advancedRocketry:tile.atmosphereDetector>);
// --- Warp Controller
recipes.remove (<advancedRocketry:tile.stationmonitor>);
// --- Ore Scanner
recipes.remove (<advancedRocketry:oreScanner>);
// --- Orientation Controller
recipes.remove (<advancedRocketry:tile.orientationControl>);
// --- Altitude Controller
recipes.remove (<advancedRocketry:tile.alititudeController>);
// --- Satellite Builder
recipes.remove (<advancedRocketry:blockSatelliteBuilder>);
// --- Composition Sensor
recipes.remove (<advancedRocketry:satellitePrimaryFunction:1>);
// --- Microwave Transmitter
recipes.remove (<advancedRocketry:satellitePrimaryFunction:3>);
// --- Mass Detector
recipes.remove (<advancedRocketry:satellitePrimaryFunction:2>);
// --- Planet ID chip
recipes.remove (<advancedRocketry:item.planetIdChip>);
// --- Space Station ID chip
recipes.remove (<advancedRocketry:item.stationChip>);
// --- Satellite ID chip
recipes.remove (<advancedRocketry:item.satelliteIdChip>);
// --- Space Station Assembler
recipes.remove (<advancedRocketry:tile.stationAssembler>);
// --- Warp Core
recipes.remove (<advancedRocketry:tile.warpCore>);
// --- Space Elevator
recipes.remove (<advancedRocketry:tile.spaceElevatorController>);
// --- Microwave Reciever
recipes.remove (<advancedRocketry:tile.microwaveReciever>);
// --- Basic Lens
recipes.remove (<advancedRocketry:lens>);
// --- Optical Sensor
recipes.remove (<advancedRocketry:satellitePrimaryFunction>);
// --- Solar Panel
recipes.remove (<advancedRocketry:tile.solarPanel>);
// --- Suit Workstation
recipes.remove (<advancedRocketry:tile.suitWorkStation>);
// --- Space Helmet
recipes.remove (<advancedRocketry:item.spaceHelmet>);
// --- Space Suit Chest-Piece
recipes.remove (<advancedRocketry:item.spaceChest>);
// --- Space Suit Legging
recipes.remove (<advancedRocketry:item.spaceLeggings>);
// --- Space boots
recipes.remove (<advancedRocketry:item.spaceBoots>);


//**************************
// Adding back recipes
//**************************


// --- Launch Pad
recipes.addShaped(<advancedRocketry:launchpad> * 6,
	[[<libVulpes:libVulpesproductsheet:6>, <libVulpes:libVulpesproductsheet:6>, <libVulpes:libVulpesproductsheet:6>],
	[<IC2:itemPartAlloy>, <IC2:itemPartAlloy>, <IC2:itemPartAlloy>],	
	[<minecraft:iron_block>, <minecraft:iron_block>, <minecraft:iron_block>]]);
	
// --- Liquid Fueled Engine
recipes.addShaped(<advancedRocketry:rocketmotor>,
	[[<gregtech:gt.metaitem.01:32731>, <gregtech:gt.metaitem.01:32476>, <minecraft:stone_button>],
	[<ore:plateTitanium>, <ore:plateSteel>, <ore:plateTitanium>],
	[<ore:plateSteel>, null, <ore:plateSteel>]]);
	
// --- Advanced Liquid Fueled Engine
recipes.addShaped(<advancedRocketry:tile.advRocket>,
	[[<advancedRocketry:advancedRocketryproductrod>, <advancedRocketry:fuelTank>, <advancedRocketry:advancedRocketryproductrod>],
	[<advancedRocketry:advancedRocketryproductsheet>, <advancedRocketry:rocketmotor>, <advancedRocketry:advancedRocketryproductsheet>],
	[<advancedRocketry:advancedRocketryproductsheet:1>, null, <advancedRocketry:advancedRocketryproductsheet:1>]]);
	
// --- Seat
recipes.addShapedMirrored(<advancedRocketry:seat>,
	[[<libVulpes:libVulpesproductsheet:6>, <IC2:blockRubber>, null],
	[<libVulpes:libVulpesproductsheet:6>, <IC2:blockRubber>, <IC2:blockRubber>],
	[<libVulpes:libVulpesproductsheet:6>, <libVulpes:libVulpesproductsheet:6>, <libVulpes:libVulpesproductsheet:6>]]);
	
// --- Machine Structure
recipes.addShaped(<libVulpes:blockStructureBlock> * 4,
	[[<ore:stickSteel>, <libVulpes:libVulpesproductsheet:6>, <ore:stickSteel>],
	[<libVulpes:libVulpesproductsheet:6>, <ore:craftingToolWrench>, <libVulpes:libVulpesproductsheet:6>],
	[<ore:stickSteel>, <libVulpes:libVulpesproductsheet:6>, <ore:stickSteel>]]);
	
// --- Advanced Machine Structure
recipes.addShaped(<libVulpes:tile.advStructureMachine> * 4,
	[[<ore:stickTitanium>, <libVulpes:libVulpesproductsheet:7>, <ore:stickTitanium>],
	[<libVulpes:libVulpesproductsheet:7>, <ore:craftingToolWrench>, <libVulpes:libVulpesproductsheet:7>],
	[<ore:stickTitanium>, <libVulpes:libVulpesproductsheet:7>, <ore:stickTitanium>]]);
	
// --- Rocket Assembling Machine
recipes.addShaped(<advancedRocketry:rocketBuilder>,
	[[<ore:stickTitanium>, <gregtech:gt.metaitem.01:32740>, <ore:stickTitanium>],
	[<advancedRocketry:circuitIC:3>, <libVulpes:blockStructureBlock>, <advancedRocketry:circuitIC:3>],
	[<ore:gearTitanium>, <ore:stoneConcrete>, <ore:gearTitanium>]]);
	
// --- Observatory
recipes.addShaped(<advancedRocketry:observatory>,
	[[<ore:paneGlass>, <gregtech:gt.metaitem.01:32740>, <ore:paneGlass>],
	[null, <libVulpes:blockStructureBlock>, null],
	[<ore:stickSteel>, <ore:stickSteel>, <ore:stickSteel>]]);
	
// --- Fueling Station
recipes.addShaped(<advancedRocketry:fuelingStation>,
	[[<libVulpes:blockStructureBlock>, <gregtech:gt.metaitem.01:32740>, <libVulpes:blockStructureBlock>],
	[<advancedRocketry:circuitIC:5>, <libVulpes:blockStructureBlock>, <libVulpes:libVulpesproductfan:6>],
	[<ore:plateTin>, <ore:plateTin>, <ore:plateTin>]]);
	
// --- Satellite Terminal
recipes.addShaped(<advancedRocketry:tile.satelliteMonitor>,
	[[<advancedRocketry:satellitePrimaryFunction>, <gregtech:gt.metaitem.01:32740>, <advancedRocketry:satellitePrimaryFunction>],
	[<ore:stickCopper>, <libVulpes:blockStructureBlock>, <ore:stickCopper>],
	[<minecraft:repeater>, <libVulpes:battery>, <minecraft:repeater>]]);
	
// --- Astrobody Data Processor
recipes.addShaped(<advancedRocketry:tile.planetanalyser>,
	[[<advancedRocketry:circuitIC:1>, <gregtech:gt.metaitem.01:32740>, <advancedRocketry:circuitIC:1>],
	[<ore:plateTin>, <libVulpes:blockStructureBlock>, <ore:plateTin>],
	[<advancedRocketry:item.planetIdChip>, <ore:plateTin>, <advancedRocketry:item.planetIdChip>]]);
	
// --- Gas Charge Pad
recipes.addShaped(<advancedRocketry:tile.oxygenCharger>,
	[[<libVulpes:libVulpesproductfan:6>, <gregtech:gt.metaitem.01:32740>, <libVulpes:libVulpesproductfan:6>],
	[<advancedRocketry:fuelTank>, <libVulpes:blockStructureBlock>, <advancedRocketry:fuelTank>],
	[<ore:plateSteel>, <minecraft:heavy_weighted_pressure_plate>, <ore:plateSteel>]]);
	
// --- Atmosphere Detector
recipes.addShaped(<advancedRocketry:tile.atmosphereDetector>,
	[[<ore:plateSteel>, <gregtech:gt.metaitem.01:32740>, <ore:plateSteel>],
	[<minecraft:iron_bars>, <libVulpes:blockStructureBlock>, <libVulpes:libVulpesproductfan:6>],
	[<ore:plateSteel>, <ore:circuitGood>, <ore:plateSteel>]]);
	
// --- Warp Controller
recipes.addShaped(<advancedRocketry:tile.stationmonitor>,
	[[<ore:plateSteel>, <gregtech:gt.metaitem.01:32740>, <ore:plateSteel>],
	[<advancedRocketry:circuitIC:3>, <libVulpes:blockStructureBlock>, <advancedRocketry:circuitIC:3>],
	[<ore:plateSteel>, <ore:circuitAdvanced>, <ore:plateSteel>]]);
	
// --- Ore Scanner
recipes.addShaped(<advancedRocketry:oreScanner>,
	[[<minecraft:lever>, <ore:circuitAdvanced>, <minecraft:lever>],
	[<libVulpes:battery>, <gregtech:gt.metaitem.01:32740>, <libVulpes:battery>],
	[null, null, null]]);
	
// --- Orientation Controller
recipes.addShapeless(<advancedRocketry:tile.orientationControl>,
	[<libVulpes:blockStructureBlock>, <minecraft:compass>, <gregtech:gt.metaitem.01:32740>]);
	
// --- Altitude Controller
recipes.addShapeless(<advancedRocketry:tile.alititudeController>,
	[<libVulpes:blockStructureBlock>, <gregtech:gt.metaitem.01:32740>, <ore:circuitGood>]);
	
// --- Satellite Builder
recipes.addShaped(<advancedRocketry:blockSatelliteBuilder>,
	[[<advancedRocketry:dataUnit>, <minecraft:hopper>, <ore:plateTitanium>],
	[<ore:circuitGood>, <libVulpes:blockStructureBlock>, <ore:circuitGood>],
	[<libVulpes:tile.motor>, <minecraft:anvil>, <advancedRocketry:tile.sawBlade>]]);
	
// --- Composition Sensor
recipes.addShaped(<advancedRocketry:satellitePrimaryFunction:1>,
	[[<advancedRocketry:satellitePrimaryFunction>, <advancedRocketry:circuitIC:1>, <advancedRocketry:satellitePrimaryFunction>],
	[<gregtech:gt.metaitem.03:32033>, <ore:circuitGood>, <gregtech:gt.metaitem.03:32033>],
	[null, null, null]]);
	
// --- Microwave Transmitter
recipes.addShaped(<advancedRocketry:satellitePrimaryFunction:3>,
	[[<gregtech:gt.metaitem.01:24890>, <advancedRocketry:circuitIC:1>, <gregtech:gt.metaitem.01:24890>],
	[<gregtech:gt.metaitem.03:32033>, <ore:circuitGood>, <gregtech:gt.metaitem.03:32033>],
	[null, null, null]]);
	
// --- Mass Detector
recipes.addShaped(<advancedRocketry:satellitePrimaryFunction:2>,
	[[<advancedRocketry:satellitePrimaryFunction>, <libVulpes:libVulpesproductcrystal>, <advancedRocketry:satellitePrimaryFunction>],
	[<gregtech:gt.metaitem.03:32033>, <ore:circuitGood>, <gregtech:gt.metaitem.03:32033>],
	[null, null, null]]);
	
// --- Planet ID chip
recipes.addShapeless(<advancedRocketry:item.planetIdChip>,
	[<ore:circuitGood>, <ore:circuitGood>, <advancedRocketry:item.satelliteIdChip>]);
	
// --- Space Station ID chip
recipes.addShapeless(<advancedRocketry:item.stationChip>,
	[<libVulpes:Linker>, <ore:circuitGood>]);
	
// --- Satellite ID chip
recipes.addShapeless(<advancedRocketry:item.satelliteIdChip>,
	[<ore:circuitGood>]);
	
// --- Space Station Assembler
recipes.addShaped(<advancedRocketry:tile.stationAssembler>,
	[[<ore:gearTitanium>, <ore:dustDilithium>, <ore:gearTitanium>],
	[<ore:dustDilithium>, <advancedRocketry:rocketBuilder>, <ore:dustDilithium>],
	[<ore:circuitAdvanced>, <ore:dustDilithium>, <ore:circuitAdvanced>]]);
	
// --- Warp Core
recipes.addShaped(<advancedRocketry:tile.warpCore>,
	[[<ore:plateTitanium>, <ore:circuitAdvanced>, <ore:plateTitanium>],
	[<ore:plateSteel>, <libVulpes:libVulpescoil0:4>, <ore:plateSteel>],
	[<ore:plateTitanium>, <ore:circuitAdvanced>, <ore:plateTitanium>]]);
	
// --- Space Elevator
recipes.addShaped(<advancedRocketry:tile.spaceElevatorController>,
	[[null, <advancedRocketry:circuitIC:3>, null],
	[<ore:circuitAdvanced>, <libVulpes:tile.advStructureMachine>, <ore:circuitAdvanced>],
	[<libVulpes:libVulpescoil0:9>, <libVulpes:libVulpescoil0:9>, <libVulpes:libVulpescoil0:9>]]);
	
// --- Microwave Reciever
recipes.addShaped(<advancedRocketry:tile.microwaveReciever>,
	[[<ore:plateGold>, <ore:plateGold>, <ore:plateGold>],
	[<advancedRocketry:circuitIC:1>, <libVulpes:blockStructureBlock>, <advancedRocketry:circuitIC:3>],
	[<ore:circuitAdvanced>, <advancedRocketry:satellitePrimaryFunction>, <ore:circuitAdvanced>]]);
	
// --- Suit Workstation
recipes.addShaped(<advancedRocketry:tile.suitWorkStation>,
	[[<gregtech:gt.metaitem.01:32650>, <ore:circuitBasic>, <gregtech:gt.metaitem.01:32650>],
	[<gregtech:gt.metaitem.01:32630>, <libVulpes:blockStructureBlock>, <gregtech:gt.metaitem.01:32630>],
	[<advancedRocketry:tile.energyPipe>, <minecraft:crafting_table>, <advancedRocketry:tile.energyPipe>]]);
	
// --- Space Helmet
recipes.addShaped(<advancedRocketry:item.spaceHelmet>,
	[[<ore:plateIron>, <ore:stickIron>, <ore:plateIron>],
	[<ore:stickIron>, <minecraft:glass_pane>, <ore:stickIron>],
	[<minecraft:wool:8>, <libVulpes:battery:1>, <minecraft:wool:8>]]);
	
// --- Space Suit Chest-Piece
recipes.addShaped(<advancedRocketry:item.spaceChest>,
	[[<minecraft:wool:8>, <libVulpes:battery:1>, <minecraft:wool:8>],
	[<ore:stickIron>, <advancedRocketry:fuelTank>, <ore:stickIron>],
	[<minecraft:wool:8>, <libVulpes:libVulpesproductfan:6>, <minecraft:wool:8>]]);
	
// --- Space Suit Legging
recipes.addShaped(<advancedRocketry:item.spaceLeggings>,
	[[<minecraft:wool:8>, <libVulpes:battery:1>, <minecraft:wool:8>],
	[<minecraft:wool:8>, <ore:stickIron>, <minecraft:wool:8>],
	[<minecraft:wool:8>, null, <minecraft:wool:8>]]);
	
// --- Space Boots
recipes.addShaped(<advancedRocketry:item.spaceBoots>,
	[[null, <ore:stickIron>, null],
	[<minecraft:wool:8>, <libVulpes:battery:1>, <minecraft:wool:8>],
	[<ore:plateIron>, null, <ore:plateIron>]]);

	
//Compressor

// --- Titanium Aluminide Block
Compressor.addRecipe(<advancedRocketry:advancedRocketrymetal0>, <advancedRocketry:advancedRocketryproductingot> * 9);

// --- Titanium Iridium Alloy Block
Compressor.addRecipe(<advancedRocketry:advancedRocketrymetal0:1>, <advancedRocketry:advancedRocketryproductingot:1> * 9);


//Mixer

// --- Dilithium Dust
//OutputStack, OutputFluid, InputArray, FluidInput, Time in Ticks, EnergyUsage
Mixer.addRecipe(<gregtech:gt.metaitem.01:2515>, null, [<appliedenergistics2:item.ItemMultiMaterial:8>, <gregtech:gt.metaitem.02:32271>], <liquid:rocket_fuel> * 1000, 100, 64);

	
//Autoclave

// --- Dilithium Crystallizer
//OutputStack, InputStack, InputFluid, OutputStackChance, Time in Ticks, EnergyUsage
Autoclave.addRecipe(<libVulpes:libVulpesproductcrystal>, <gregtech:gt.metaitem.01:2515>, <liquid:ic2distilledwater> * 1000, 10000, 400, 1536);
	
	
//Assembler

// --- Liquid Fuel Tank
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:fuelTank>, <libVulpes:libVulpesproductsheet:6> * 2, <gregtech:gt.metaitem.01:28306> * 2, null, 200, 96);

// --- Data Storage Unit
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:dataUnit>, <gregtech:gt.metaitem.01:32708>, <gregtech:gt.integrated_circuit:1> * 0, null, 100, 5);

// --- Jet Pack
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:item.jetPack>, <GraviSuite:advJetpack:*>, <gregtech:gt.integrated_circuit:1> * 0, null, 200, 90);

// --- Tracking Circuit
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:circuitIC:1>, <gregtech:gt.metaitem.03:32033>, <Railcraft:part.circuit>, null, 200, 60);

// --- Space Elevator Chip
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:item.elevatorChip>, <advancedRocketry:circuitIC:1>, <advancedRocketry:item.stationChip>, null, 400, 90);

// --- Optical Sensor
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:satellitePrimaryFunction>, <gregtech:gt.metaitem.01:32691>, <gregtech:gt.metaitem.01:24890>, null, 200, 60);

// --- Lens
//OutputStack, InputStack1, InputStack2, InputFluid, Time in Ticks, EnergyUsage
Assembler.addRecipe(<advancedRocketry:tile.lens>, <gregtech:gt.metaitem.01:24890>, <gregtech:gt.metaitem.01:28019> * 4, null, 200, 16);

	
//Centrifuge

// --- Moon Turf
//OutputArray, InputFluid, InputStack, InputCell, OutputFluid, OutputArrayChances, Time in Ticks, EnergyUsage
Centrifuge.addRecipe([<minecraft:sand>, <gregtech:gt.metaitem.01:2019>, <gregtech:gt.metaitem.01:2823>, <gregtech:gt.metaitem.01:2032>, <gregtech:gt.metaitem.01:2018>, <gregtech:gt.metaitem.01:2375>], null, <advancedRocketry:moonTurf>, null, <liquid:helium-3> * 33, [5000, 400, 400, 100, 100, 100], 400, 8);

//Fluid Extractor

// --- Oxidized Ferric Sand
//OutputStack, InputStack, OutputFluid, OutputStackChance, Time in Ticks, EnergyUsage
FluidExtractor.addRecipe(<minecraft:stone>, <advancedRocketry:blockHotTurf> , <liquid:molten.iron> * 50, 10000, 240, 16);

//Blast Furnace

// --- Titanium Aluminide Ingot
//OutputArray, InputFluid, InputArray, Time in Ticks, EnergyUsage, HeatAmount
BlastFurnace.addRecipe([<advancedRocketry:advancedRocketryproductingot> * 10], null, [<gregtech:gt.metaitem.01:11019> * 7, <gregtech:gt.metaitem.01:11028> * 3], 24000, 480, 2900);

// --- Titanium Iridium Alloy Ingot
//OutputArray, InputFluid, InputArray, Time in Ticks, EnergyUsage, HeatAmount
BlastFurnace.addRecipe([<advancedRocketry:advancedRocketryproductingot:1> * 2], null, [<gregtech:gt.metaitem.01:11028>, <gregtech:gt.metaitem.01:11084>], 3000, 480, 3100);

// --- Compressor

// --- Titanium Aluminide Block
//Compressor.addRecipe(<advancedRocketry:advancedRocketrymetal0>, <advancedRocketry:advancedRocketryproductingot> * 9);

// --- Titanium Iridium Alloy Block
//Compressor.addRecipe(<advancedRocketry:advancedRocketrymetal0:1>, <advancedRocketry:advancedRocketryproductingot:1> * 9);


//Extruder

// --- Titanium Aluminide Rod
//OutputStack, InputStack, InputShape, Time in Ticks, EnergyUsage
Extruder.addRecipe(<advancedRocketry:advancedRocketryproductrod> * 2, <advancedRocketry:advancedRocketryproductingot>, <gregtech:gt.metaitem.01:32351> * 0, 260, 288);

// --- Titanium Iridium Alloy Rod
//OutputStack, InputStack, InputShape, Time in Ticks, EnergyUsage
Extruder.addRecipe(<advancedRocketry:advancedRocketryproductrod:1> * 2, <advancedRocketry:advancedRocketryproductingot:1>, <gregtech:gt.metaitem.01:32351> * 0, 240, 396);

// --- Titanium Aluminide Plate
//OutputStack, InputStack, InputShape, Time in Ticks, EnergyUsage
Extruder.addRecipe(<advancedRocketry:advancedRocketryproductplate>, <advancedRocketry:advancedRocketryproductingot>, <gregtech:gt.metaitem.01:32350> * 0, 100, 512);

// --- Titanium Iridium Alloy Plate
//OutputStack, InputStack, InputShape, Time in Ticks, EnergyUsage
Extruder.addRecipe(<advancedRocketry:advancedRocketryproductplate:1>, <advancedRocketry:advancedRocketryproductingot>, <gregtech:gt.metaitem.01:32350> * 0, 100, 512);

//Implosion Compressor

// --- Steel Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<libVulpes:libVulpesproductsheet:6>, <gregtech:gt.metaitem.01:816>], <Railcraft:part.plate:1> * 2, 2);

// --- Iron Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<libVulpes:libVulpesproductsheet:1>, <gregtech:gt.metaitem.01:816>], <Railcraft:part.plate> * 2, 2);

// --- Aluminum Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<libVulpes:libVulpesproductsheet:9>, <gregtech:gt.metaitem.01:816>], <gregtech:gt.metaitem.01:17019> * 2, 2);

// --- Copper Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<libVulpes:libVulpesproductsheet:4>, <gregtech:gt.metaitem.01:816>], <Railcraft:part.plate:3> * 2, 2);

// --- Titanium Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<libVulpes:libVulpesproductsheet:7>, <gregtech:gt.metaitem.01:816>], <gregtech:gt.metaitem.01:17028> * 2, 2);

// --- Titanium Aluminide Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<advancedRocketry:advancedRocketryproductsheet>, <gregtech:gt.metaitem.01:816>], <advancedRocketry:advancedRocketryproductplate> * 2, 4);

// --- Titanium Iridium Alloy Sheet
//OutputArray, InputStack, TNTAmount
ImplosionCompressor.addRecipe([<advancedRocketry:advancedRocketryproductsheet:1>, <gregtech:gt.metaitem.01:816>], <advancedRocketry:advancedRocketryproductplate:1> * 2, 4);


//*****************************
// Oredictionary Modifications
//*****************************

<ore:sand>.add(<advancedRocketry:tile.vitrifiedSand>);

