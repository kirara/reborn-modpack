// --- Created by DreamMasterXXL ---
// --- Modified by OrdoFree


// --- Imports


import mods.gregtech.Assembler;
import mods.gregtech.CuttingSaw;
import mods.gregtech.ForgeHammer;



// --- Removing Recipes ---




// --- Ztones Tile
recipes.remove(<Ztones:stoneTile>);

// --- Garden Soil
recipes.remove(<Ztones:cleanDirt>);

// --- Booster
recipes.remove(<Ztones:booster>);

// --- Flat Lamp light
//recipes.remove(<Ztones:lampf>);

// --- Flat Lamp white
//recipes.remove(<Ztones:lampt>);

// --- Flat Lamp black
//recipes.remove(<Ztones:lampb>);

// --- Aurora Block
recipes.remove(<Ztones:auroraBlock>);

// --- Diamond Zane
recipes.remove(<Ztones:diamondZane>);

// --- Adding Recipes ---



// --- Ztones Tile
recipes.addShaped(<Ztones:stoneTile> * 8, [
[<minecraft:stone_slab>, <minecraft:stone_slab>, <minecraft:stone_slab>],
[<minecraft:stone_slab>, <minecraft:stone>, <minecraft:stone_slab>],
[<minecraft:stone_slab>, <minecraft:stone_slab>, <minecraft:stone_slab>]]);

// --- Aurora Block
recipes.addShaped(<Ztones:auroraBlock> * 16, [
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>],
[<ore:blockGlass>, <ore:dye>, <ore:blockGlass>],
[<ore:blockGlass>, <ore:blockGlass>, <ore:blockGlass>]]);

// --- Diamond Zane
recipes.addShaped(<Ztones:diamondZane> , [
[<ore:toolHeadPickaxeDiamond>, <gregtech:gt.metaitem.01:17322>, <ore:toolHeadAxeDiamond>],
[<gregtech:gt.metaitem.01:17322>, <ore:toolHeadShovelDiamond>, <gregtech:gt.metaitem.01:17322>],
[null, <gregtech:gt.metaitem.01:23322>, null]]);




// --- Assembler Recipes ---



// --- Ztones Tile
Assembler.addRecipe(<Ztones:stoneTile> * 8, <minecraft:stone>, <minecraft:stone_slab> * 4, 160, 4);

// --- Garden Soil
Assembler.addRecipe(<Ztones:cleanDirt> * 8, <minecraft:sand:*> * 4, <minecraft:dirt:*> * 4, <liquid:seedoil> * 5, 160, 4);

// --- Booster
Assembler.addRecipe(<Ztones:booster>, <minecraft:stone_pressure_plate>, <gregtech:gt.integrated_circuit:1> * 0,  <liquid:molten.blaze> * 8, 100, 30);


// --- Aurora Block
Assembler.addRecipe(<Ztones:auroraBlock> * 16, <minecraft:glass> * 4, <minecraft:dye:*>, 160, 4);



// --- Cutting Saw Recipes ---




// --- Flat Lamp light
//CuttingSaw.addRecipe([<Ztones:lampf> * 4], <RedLogic:redlogic.lampCubeDecorative>, <liquid:water> * 100, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampf> * 4], <RedLogic:redlogic.lampCubeDecorative>, <liquid:ic2distilledwater> * 75, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampf> * 4], <RedLogic:redlogic.lampCubeDecorative>, <liquid:lubricant> * 25, 100, 4);

// --- Flat Lamp white
//CuttingSaw.addRecipe([<Ztones:lampt> * 4], <RedLogic:redlogic.lampCubeDecorative:8>, <liquid:water> * 100, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampt> * 4], <RedLogic:redlogic.lampCubeDecorative:8>, <liquid:ic2distilledwater> * 75, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampt> * 4], <RedLogic:redlogic.lampCubeDecorative:8>, <liquid:lubricant> * 25, 100, 4);

// --- Flat Lamp black
//CuttingSaw.addRecipe([<Ztones:lampb> * 4], <RedLogic:redlogic.lampCubeDecorative:15>, <liquid:water> * 100, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampb> * 4], <RedLogic:redlogic.lampCubeDecorative:15>, <liquid:ic2distilledwater> * 75, 200, 4);
// -
//CuttingSaw.addRecipe([<Ztones:lampb> * 4], <RedLogic:redlogic.lampCubeDecorative:15>, <liquid:lubricant> * 25, 100, 4);
