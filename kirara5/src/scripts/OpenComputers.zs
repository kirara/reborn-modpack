//Created by Kosyak
 
import mods.gregtech.Assembler;
import mods.gregtech.CuttingSaw;
import mods.gregtech.FormingPress;
 
val transistor = <OpenComputers:item:23>;
val microchip1 = <OpenComputers:item:24>;
val microchip2 = <OpenComputers:item:25>;
val microchip3 = <OpenComputers:item:26>;
val rawPCB = <OpenComputers:item:30>;
val pcb = <OpenComputers:item:32>;
val cu = <OpenComputers:item:28>;
val alu = <OpenComputers:item:27>;
val cpu1 = <OpenComputers:item:29>;
val cpu2 = <OpenComputers:item:42>;
val cpu3 = <OpenComputers:item:43>;
val ram1 = <OpenComputers:item:1>;
val ram2 = <OpenComputers:item:2>;
val ram3 = <OpenComputers:item:38>;
val bus1 = <OpenComputers:item:70>;
val bus2 = <OpenComputers:item:71>;
val bus3 = <OpenComputers:item:72>;
val apu1 = <OpenComputers:item:101>;
val apu2 = <OpenComputers:item:102>;
val gpu1 = <OpenComputers:item:8>;
val gpu2 = <OpenComputers:item:9>;
val gpu3 = <OpenComputers:item:10>;
val data1 = <OpenComputers:item:104>;
val data2 = <OpenComputers:item:105>;
val data3 = <OpenComputers:item:106>;
val hdd1 = <OpenComputers:item:5>;
val hdd2 = <OpenComputers:item:6>;
val hdd3 = <OpenComputers:item:7>;
val cardBase = <OpenComputers:item:33>;
val disk = <OpenComputers:item:19>;
val droneCase1 = <OpenComputers:item:83>;
val droneCase2 = <OpenComputers:item:87>;
val mcuCase1 = <OpenComputers:item:82>;
val mcuCase2 = <OpenComputers:item:86>;
val floppy = <OpenComputers:item:4>;
val cartrige = <OpenComputers:item:94>;
val redstoneCard1 = <OpenComputers:item:66>;
val redstoneCard2 = <OpenComputers:item:12>;
val server1 = <OpenComputers:item:45>;
val server2 = <OpenComputers:item:46>;
val server3 = <OpenComputers:item:40>;
val tabletCase1 = <OpenComputers:item:74>;
val tabletCase2 = <OpenComputers:item:92>;
val screen1 = <OpenComputers:screen1>;
val screen2 = <OpenComputers:screen2>;
val screen3 = <OpenComputers:screen3>;
 
/*
val case1 = <OpenComputers:case1>;
val case2 = <OpenComputers:case2>;
val case3 = <OpenComputers:case3>;
*/
 
//val cableGt01AnyCopper = <gregtech:gt.blockmachines:1386>;
 
 
val remoteTerminal = <OpenComputers:item:41>;
val solarUpgrade = <OpenComputers:item:34>;
val wlanCard = <OpenComputers:item:13>;
val keyboard = <OpenComputers:keyboard>;
val eeprom = <OpenComputers:eeprom>;
val adapter = <OpenComputers:adapter>;
val charger = <OpenComputers:charger>;
val fdd = <OpenComputers:diskDrive>;
val geolyzer = <OpenComputers:geolyzer>;
val hologram1 = <OpenComputers:hologram1>;
val hologram2 = <OpenComputers:hologram2>;
val motionSensor = <OpenComputers:motionSensor>;
val powerConverter = <OpenComputers:powerConverter>;
val powerDistributor = <OpenComputers:powerDistributor>;
val raid = <OpenComputers:raid>;
val rsio = <OpenComputers:redstone>;
val relay = <OpenComputers:relay>;
val netCard = <OpenComputers:item:11>;
val rack = <OpenComputers:rack>;
val printer3D = <OpenComputers:printer>;
val waypoint = <OpenComputers:waypoint>;
val interweb = <OpenComputers:item:48>;
val splitter = <OpenComputers:netSplitter>;
val transposer = <OpenComputers:transposer>;
val capacitor = <OpenComputers:capacitor>;
val analyzer = <OpenComputers:item>;
 
val nandChip = <gregtech:gt.metaitem.01:32700>;
val basicCircuitBoard = <gregtech:gt.metaitem.01:32710>;
val advancedCircuitBoard = <gregtech:gt.metaitem.01:32711>;
val processorBoard = <gregtech:gt.metaitem.01:32712>;
 
val etchedLVWire = <gregtech:gt.metaitem.01:32716>;
 
val goldWire = <gregtech:gt.blockmachines:1420>;
val copperWire = <gregtech:gt.blockmachines:1360>;
val electrumWire = <gregtech:gt.blockmachines:1440>;
val tinWire = <gregtech:gt.blockmachines:1240>;
val redAllowWire = <gregtech:gt.blockmachines:2000>;
val redAllowWire8 = <gregtech:gt.blockmachines:2003>;
val cupronickelWire2 = <gregtech:gt.blockmachines:1341>;
 
val electrumPipe = <gregtech:gt.blockmachines:5612>;
val ssteelPipe = <gregtech:gt.blockmachines:5142>;
 
val finePlatinumWire = <gregtech:gt.metaitem.02:19085>;
val fineElectrumWire = <gregtech:gt.metaitem.02:19303>;
val fineGoldWire = <gregtech:gt.metaitem.02:19086>;
val fineCopperWire = <gregtech:gt.metaitem.02:19345>;
 
val goodCircuit = <gregtech:gt.metaitem.01:32702>;
val evCircuit = <gregtech:gt.metaitem.01:32705>;
 
val gtScanner = <gregtech:gt.metaitem.01:32762>;
 
val copperCable = <gregtech:gt.blockmachines:1366>;
val goldCable = <gregtech:gt.blockmachines:1426>;
val aluminiumCable = <gregtech:gt.blockmachines:1586>;
 
val gtAssembler = <gregtech:gt.blockmachines:212>;
val gtDisassembler = <gregtech:gt.blockmachines:452>;
val gtCharger = <gregtech:gt.blockmachines:692>;
val gtPrinter = <gregtech:gt.blockmachines:323>;
val gtChestBuffer = <gregtech:gt.blockmachines:9232>;
val gtGenerator = <gregtech:gt.blockmachines:1111>;
 
val gtCapacitor = <gregtech:gt.metaitem.01:32499>;
 
val mvHull = <gregtech:gt.blockmachines:12>;
val hvHull = <gregtech:gt.blockmachines:13>;
val evHull = <gregtech:gt.blockmachines:14>;
 
val mvTransformer = <gregtech:gt.blockmachines:22>;
 
val mvRotor = <gregtech:gt.metaitem.02:21300>;
val hvRotor = <gregtech:gt.metaitem.02:21305>;
val evRotor = <gregtech:gt.metaitem.02:21306>;
val carbonRotor = <gregtech:gt.metaitem.02:21010>;
 
val mvMotor = <gregtech:gt.metaitem.01:32601>;
val hvMotor = <gregtech:gt.metaitem.01:32602>;
val evMotor = <gregtech:gt.metaitem.01:32603>;
val mvPump = <gregtech:gt.metaitem.01:32611>;
val hvPump = <gregtech:gt.metaitem.01:32612>;
val mvPiston = <gregtech:gt.metaitem.01:32641>;
val hvPiston = <gregtech:gt.metaitem.01:32642>;
val evPiston = <gregtech:gt.metaitem.01:32643>;
val mvArm = <gregtech:gt.metaitem.01:32651>;
val hvArm = <gregtech:gt.metaitem.01:32652>;
val evArm = <gregtech:gt.metaitem.01:32653>;
 
val mvConv = <gregtech:gt.metaitem.01:32631>;
val hvConv = <gregtech:gt.metaitem.01:32632>;
 
val solarPanel = <gregtech:gt.metaitem.01:32750>;
val steelCell = <gregtech:gt.metaitem.01:32405>;
val shutter = <gregtech:gt.metaitem.01:32749>;
 
val hvEmitter = <gregtech:gt.metaitem.01:32682>;
val evEmitter = <gregtech:gt.metaitem.01:32683>;
 
val hvSensor = <gregtech:gt.metaitem.01:32692>;
val evSensor = <gregtech:gt.metaitem.01:32693>;
 
val lvFieldGenerator = <gregtech:gt.metaitem.01:32670>;
 
val ironScrew = <gregtech:gt.metaitem.01:27032>;
val ironPlate = <ore:plateAnyIron>;
 
val magneticPlate = <gregtech:gt.metaitem.01:17355>;
val magneticRing = <gregtech:gt.metaitem.01:28355>;
 
val polyethylenPlate = <gregtech:gt.metaitem.01:17874>;
 
val AlPlate = <gregtech:gt.metaitem.01:17019>;
val SsPlate = <gregtech:gt.metaitem.01:17306>;
val TiPlate = <gregtech:gt.metaitem.01:17028>;
 
val steelCasing = <IC2:itemCasing:5>;
val ventSpread = <IC2:reactorVentSpread>;
val ventGold = <IC2:reactorVentGold:1>;
val ventDiamond = <IC2:reactorVentDiamond:1>;
 
val dataControlCircuit = <gregtech:gt.metaitem.01:32705>;
 
val monitor = <gregtech:gt.metaitem.01:32740>;
 
//recipes.remove(transistor);
//recipes.remove(microchip1);
//recipes.remove(microchip2);
//recipes.remove(microchip3);
recipes.remove(rawPCB);
furnace.remove(pcb);
 
// Transistor
 
recipes.addShaped(transistor,
                  [[transistor, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>]]);
 
recipes.addShaped(<OpenComputers:cable>,
                  [[<OpenComputers:cable>, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>]]);
recipes.addShaped(microchip1,
                  [[microchip1, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>],
                   [<minecraft:paper>, <minecraft:paper>, <minecraft:paper>]]);
 
 
 
// Cable - TODO: better recipe
//recipes.remove(<OpenComputers:cable>);
Assembler.addRecipe(<OpenComputers:cable>, fineCopperWire * 4, <minecraft:string> * 4, <liquid:molten.rubber> * 144, 100, 8);
 
//Raw PCB - TODO: better recipe
CuttingSaw.addRecipe([rawPCB * 4], <minecraft:stained_hardened_clay:13>, <liquid:lubricant> * 1, 200, 16);
CuttingSaw.addRecipe([rawPCB * 4], <minecraft:stained_hardened_clay:13>, <liquid:ic2distilledwater> * 3, 400, 16);
CuttingSaw.addRecipe([rawPCB * 4], <minecraft:stained_hardened_clay:13>, <liquid:water> * 5, 400, 16);
 
// Server Rack
recipes.remove(rack);
recipes.addShaped(rack,
                  [[microchip2, wlanCard, microchip2],
                   [hvRotor, hvHull, hvRotor],
                   [relay, pcb, powerDistributor]]);
 
 
// PCB
FormingPress.addRecipe(pcb, rawPCB, etchedLVWire * 4, 32, 16);
 
// Capacitor
recipes.remove(<OpenComputers:capacitor>);
recipes.addShaped(<OpenComputers:capacitor>,
                  [[gtCapacitor, goodCircuit, gtCapacitor],
                   [transistor, mvHull, transistor],
                   [gtCapacitor, copperCable, gtCapacitor]]);

// Capacitor
recipes.addShaped(<OpenComputers:capacitor>,
                  [[gtCapacitor, <gregtech:gt.metaitem.03:32080>, gtCapacitor],
                   [transistor, mvHull, transistor],
                   [gtCapacitor, copperCable, gtCapacitor]]);
recipes.addShaped(<OpenComputers:capacitor>,
                  [[gtCapacitor, <gregtech:gt.metaitem.03:32079>, gtCapacitor],
                   [transistor, mvHull, transistor],
                   [gtCapacitor, copperCable, gtCapacitor]]);
recipes.addShaped(<OpenComputers:capacitor>,
                  [[gtCapacitor, <gregtech:gt.metaitem.01:32702>, gtCapacitor],
                   [transistor, mvHull, transistor],
                   [gtCapacitor, copperCable, gtCapacitor]]);
 
 
// Casing tier 1
recipes.remove(<OpenComputers:case1>);
recipes.addShaped(<OpenComputers:case1>,
                  [[microchip1, goodCircuit, microchip1],
                   [ventSpread, mvHull, ventSpread],
                   [copperCable, pcb, copperCable]]);
// Casing tier 1
recipes.remove(<OpenComputers:case1>);
recipes.addShaped(<OpenComputers:case1>,
                  [[microchip1, <gregtech:gt.metaitem.03:32080>, microchip1],
                   [ventSpread, mvHull, ventSpread],
                   [copperCable, pcb, copperCable]]);
recipes.addShaped(<OpenComputers:case1>,
                  [[microchip1, <gregtech:gt.metaitem.03:32079>, microchip1],
                   [ventSpread, mvHull, ventSpread],
                   [copperCable, pcb, copperCable]]);
recipes.addShaped(<OpenComputers:case1>,
                  [[microchip1, <gregtech:gt.metaitem.01:32702>, microchip1],
                   [ventSpread, mvHull, ventSpread],
                   [copperCable, pcb, copperCable]]);
// Casing tier 2
recipes.remove(<OpenComputers:case2>);
recipes.addShaped(<OpenComputers:case2>,
                  [[microchip2, <IC2:itemPartCircuitAdv>, microchip2],
                   [ventGold, hvHull, ventGold],
                   [goldCable, pcb, goldCable]]);
// Casing tier 3
recipes.remove(<OpenComputers:case3>);
recipes.addShaped(<OpenComputers:case3>,
                  [[microchip3, evCircuit, microchip3],
                   [ventDiamond, evHull, ventDiamond],
                   [aluminiumCable, pcb, aluminiumCable]]);
// Casing tier 3
recipes.remove(<OpenComputers:case3>);
recipes.addShaped(<OpenComputers:case3>,
                  [[microchip3, <gregtech:gt.metaitem.03:32085>, microchip3],
                   [ventDiamond, evHull, ventDiamond],
                   [aluminiumCable, pcb, aluminiumCable]]);
recipes.addShaped(<OpenComputers:case3>,
                  [[microchip3, <gregtech:gt.metaitem.03:32083>, microchip3],
                   [ventDiamond, evHull, ventDiamond],
                   [aluminiumCable, pcb, aluminiumCable]]);
recipes.addShaped(<OpenComputers:case3>,
                  [[microchip3, <gregtech:gt.metaitem.01:32705>, microchip3],
                   [ventDiamond, evHull, ventDiamond],
                   [aluminiumCable, pcb, aluminiumCable]]); 
// Floppy
//recipes.remove(floppy);
 
recipes.addShaped(floppy,
                  [[polyethylenPlate, <minecraft:paper>, polyethylenPlate],
                   [<minecraft:paper>, disk, <minecraft:paper>],
                   [polyethylenPlate, <IC2:itemCasing:5>, polyethylenPlate]]);
 
// EEPROM
//recipes.remove(eeprom);
 
recipes.addShaped(eeprom,
                  [[polyethylenPlate, <minecraft:paper>, polyethylenPlate],
                   [nandChip, microchip1, nandChip],
                   [polyethylenPlate, fineGoldWire, polyethylenPlate]]);
