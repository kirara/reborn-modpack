// --- Created by Zulfurlubak for KR
// Sorry in advance, I know it's completely half-assed way to do it, but adding recipes with "*" simply didn't work

import mods.chisel.Groups;

# Chiseling fixes
mods.chisel.Groups.removeGroup("present");
mods.chisel.Groups.addGroup("present");
mods.chisel.Groups.addVariation("present", <minecraft:chest>);
mods.chisel.Groups.addVariation("present", <chisel:present>);
mods.chisel.Groups.addVariation("present", <chisel:present:1>);
mods.chisel.Groups.addVariation("present", <chisel:present:2>);
mods.chisel.Groups.addVariation("present", <chisel:present:3>);
mods.chisel.Groups.addVariation("present", <chisel:present:4>);
mods.chisel.Groups.addVariation("present", <chisel:present:5>);
mods.chisel.Groups.addVariation("present", <chisel:present:6>);
mods.chisel.Groups.addVariation("present", <chisel:present:7>);
mods.chisel.Groups.addVariation("present", <chisel:present:8>);
mods.chisel.Groups.addVariation("present", <chisel:present:9>);
mods.chisel.Groups.addVariation("present", <chisel:present:10>);
mods.chisel.Groups.addVariation("present", <chisel:present:11>);
mods.chisel.Groups.addVariation("present", <chisel:present:12>);
mods.chisel.Groups.addVariation("present", <chisel:present:13>);
mods.chisel.Groups.addVariation("present", <chisel:present:14>);
mods.chisel.Groups.addVariation("present", <chisel:present:15>);
