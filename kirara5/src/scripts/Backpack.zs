// --- Created by Tiger --- 

# Aliases

var packSmall = <Backpack:backpack>;
var packSmallBlack = <Backpack:backpack:1>;
var packSmallRed = <Backpack:backpack:2>;
var packSmallGreen = <Backpack:backpack:3>;
var packSmallBrown = <Backpack:backpack:4>;
var packSmallBlue = <Backpack:backpack:5>;
var packSmallPurple = <Backpack:backpack:6>;
var packSmallCyan = <Backpack:backpack:7>;
var packSmallLightGrey = <Backpack:backpack:8>;
var packSmallGrey = <Backpack:backpack:9>;
var packSmallPink = <Backpack:backpack:10>;
var packSmallLime = <Backpack:backpack:11>;
var packSmallYellow = <Backpack:backpack:12>;
var packSmallLightBlue = <Backpack:backpack:13>;
var packSmallMagenta = <Backpack:backpack:14>;
var packSmallOrange = <Backpack:backpack:15>;
var packSmallWhite = <Backpack:backpack:16>;

var packMedium = <Backpack:backpack:100>;
var packMediumBlack = <Backpack:backpack:101>;
var packMediumRed = <Backpack:backpack:102>;
var packMediumGreen = <Backpack:backpack:103>;
var packMediumBrown = <Backpack:backpack:104>;
var packMediumBlue = <Backpack:backpack:105>;
var packMediumPurple = <Backpack:backpack:106>;
var packMediumCyan = <Backpack:backpack:107>;
var packMediumLightGrey = <Backpack:backpack:108>;
var packMediumGrey = <Backpack:backpack:109>;
var packMediumPink = <Backpack:backpack:110>;
var packMediumLime = <Backpack:backpack:111>;
var packMediumYellow = <Backpack:backpack:112>;
var packMediumLightBlue = <Backpack:backpack:113>;
var packMediumMagenta = <Backpack:backpack:114>;
var packMediumOrange = <Backpack:backpack:115>;
var packMediumWhite = <Backpack:backpack:116>;

var packLarge = <Backpack:backpack:200>;
var packLargeBlack = <Backpack:backpack:201>;
var packLargeRed = <Backpack:backpack:202>;
var packLargeGreen = <Backpack:backpack:203>;
var packLargeBrown = <Backpack:backpack:204>;
var packLargeBlue = <Backpack:backpack:205>;
var packLargePurple = <Backpack:backpack:206>;
var packLargeCyan = <Backpack:backpack:207>;
var packLargeLightGrey = <Backpack:backpack:208>;
var packLargeGrey = <Backpack:backpack:209>;
var packLargePink = <Backpack:backpack:210>;
var packLargeLime = <Backpack:backpack:211>;
var packLargeYellow = <Backpack:backpack:212>;
var packLargeLightBlue = <Backpack:backpack:213>;
var packLargeMagenta = <Backpack:backpack:214>;
var packLargeOrange = <Backpack:backpack:215>;
var packLargeWhite = <Backpack:backpack:216>;

var dyeBlack = <minecraft:dye:0>;
var dyeRed = <minecraft:dye:1>;
var dyeGreen = <minecraft:dye:2>;
var dyeBrown = <minecraft:dye:3>;
var dyeBlue = <minecraft:dye:4>;
var dyePurple = <minecraft:dye:5>;
var dyeCyan = <minecraft:dye:6>;
var dyeLightGrey = <minecraft:dye:7>;
var dyeGrey = <minecraft:dye:8>;
var dyePink = <minecraft:dye:9>;
var dyeLime = <minecraft:dye:10>;
var dyeYellow = <minecraft:dye:11>;
var dyeLightBlue = <minecraft:dye:12>;
var dyeMagenta = <minecraft:dye:13>;
var dyeOrange = <minecraft:dye:14>;
var dyeWhite = <minecraft:dye:15>;

var boundLeather = <Backpack:boundLeather>;
var tannedLeather = <Backpack:tannedLeather>;
var plateSteel = <Railcraft:part.plate:1>;
var plateTitanium = <gregtech:gt.metaitem.01:17028>;
var chest = <minecraft:chest>;


# Recipe Tweaks

# Small Bags

recipes.remove(<Backpack:backpack>);
recipes.addShaped(packSmall, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, null, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:1>);
recipes.addShaped(packSmallBlack, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeBlack, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:2>);
recipes.addShaped(packSmallRed, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeRed, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);

recipes.remove(<Backpack:backpack:3>);
recipes.addShaped(packSmallGreen, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeGreen, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);

recipes.remove(<Backpack:backpack:4>);
recipes.addShaped(packSmallBrown, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeBrown, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);

recipes.remove(<Backpack:backpack:5>);
recipes.addShaped(packSmallBlue, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeBlue, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);

recipes.remove(<Backpack:backpack:6>);
recipes.addShaped(packSmallPurple, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyePurple, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);	

recipes.remove(<Backpack:backpack:7>);
recipes.addShaped(packSmallCyan, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeCyan, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:8>);
recipes.addShaped(packSmallLightGrey, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeLightGrey, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:9>);
recipes.addShaped(packSmallGrey, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeGrey, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:10>);
recipes.addShaped(packSmallPink, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyePink, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:11>);
recipes.addShaped(packSmallLime, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeLime, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:12>);
recipes.addShaped(packSmallYellow, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeYellow, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:13>);
recipes.addShaped(packSmallLightBlue, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeLightBlue, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:14>);
recipes.addShaped(packSmallMagenta, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeMagenta, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:15>);
recipes.addShaped(packSmallOrange, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeOrange, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	
recipes.remove(<Backpack:backpack:16>);
recipes.addShaped(packSmallWhite, [
	[tannedLeather, tannedLeather, tannedLeather],
	[tannedLeather, dyeWhite, tannedLeather],
	[tannedLeather, chest, tannedLeather]]);
	

# Medium Bags

recipes.remove(<Backpack:backpack:100>);
recipes.addShaped(packMedium, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, null, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:101>);
recipes.addShaped(packMediumBlack, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeBlack, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:102>);
recipes.addShaped(packMediumRed, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeRed, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:103>);
recipes.addShaped(packMediumGreen, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeGreen, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:104>);
recipes.addShaped(packMediumBrown, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeBrown, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:105>);
recipes.addShaped(packMediumBlue, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeBlue, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:106>);
recipes.addShaped(packMediumPurple, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyePurple, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:107>);
recipes.addShaped(packMediumCyan, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeCyan, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:108>);
recipes.addShaped(packMediumLightGrey, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeLightGrey, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:109>);
recipes.addShaped(packMediumGrey, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeGrey, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:110>);
recipes.addShaped(packMediumPink, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyePink, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:111>);
recipes.addShaped(packMediumLime, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeLime, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:112>);
recipes.addShaped(packMediumYellow, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeYellow, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:113>);
recipes.addShaped(packMediumLightBlue, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeLightBlue, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:114>);
recipes.addShaped(packMediumMagenta, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeMagenta, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:115>);
recipes.addShaped(packMediumOrange, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeOrange, plateSteel],
	[plateSteel, chest, plateSteel]]);
	
recipes.remove(<Backpack:backpack:116>);
recipes.addShaped(packMediumWhite, [
	[plateSteel, plateSteel, plateSteel],
	[plateSteel, dyeWhite, plateSteel],
	[plateSteel, chest, plateSteel]]);
	

# Large Bags
	
recipes.remove(<Backpack:backpack:200>);
recipes.addShaped(packLarge, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, null, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:201>);
recipes.addShaped(packLargeBlack, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeBlack, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:202>);
recipes.addShaped(packLargeRed, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeRed, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:203>);
recipes.addShaped(packLargeGreen, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeGreen, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:204>);
recipes.addShaped(packLargeBrown, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeBrown, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:205>);
recipes.addShaped(packLargeBlue, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeBlue, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:206>);
recipes.addShaped(packLargePurple, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyePurple, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:207>);
recipes.addShaped(packLargeCyan, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeCyan, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:208>);
recipes.addShaped(packLargeLightGrey, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeLightGrey, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:209>);
recipes.addShaped(packLargeGrey, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeGrey, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:210>);
recipes.addShaped(packLargePink, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyePink, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:211>);
recipes.addShaped(packLargeLime, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeLime, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:212>);
recipes.addShaped(packLargeYellow, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeYellow, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:213>);
recipes.addShaped(packLargeLightBlue, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeLightBlue, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:214>);
recipes.addShaped(packLargeMagenta, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeMagenta, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:215>);
recipes.addShaped(packLargeOrange, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeOrange, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	
recipes.remove(<Backpack:backpack:216>);
recipes.addShaped(packLargeWhite, [
	[plateTitanium, plateTitanium, plateTitanium],
	[plateTitanium, dyeWhite, plateTitanium],
	[plateTitanium, chest, plateTitanium]]);
	

	

