# To use different sets of recipes, include other recipe files in the order of
# priority to use the recipes defined in them. The last include has the highest
# priority (i.e. included recipes simply replace the current definition for all
# already known recipes).

# To disable a recipe, assign a boolean value to it. For example, to disable
# the recipe for the "oc:materialTransistor": `"oc:materialTransistor" = false`. This will disable the
# recipe and hide the item in the creative tab and NEI. If you assign `true`,
# the recipe will still be disabled, but not hidden in the creative tab/NEI.

#include file("default.recipes")
include file("hardmode.recipes")
#include file("gregtech.recipes")
#include file("peaceful.recipes")
#include file("your_custom.recipes")

# You can also specify custom recipes in this file directly. Have a look at the
# default.recipes file to get an idea how recipes have to be structured. You'll
# want to define your recipes after all includes, to avoid those overwriting
# your recipes.

# Kirara recipes

transistor {
  type: shapeless
  input: ["oc:capacitor", wireGt01RedAlloy]
}
transistor {
  type: gt_assembler
  input: [{item="ic2.itemCasing", subID=4}, wireGt01RedAlloy]
  count: [1, 1]
  eu: 16
  time: 32
  output: 1
  inputFluid: {name="molten.solderingalloy", amount="18"}
}
chip1 {
  type: gt_assembler
  input: [{item="gt.metaitem.01", subID=32710}, "oc:materialTransistor"]
  count: [1, 2]
  eu: 16
  time: 32
  output: 1
  inputFluid: {name="molten.solderingalloy", amount="36"}
}
chip2 {
  type: gt_assembler
  input: [{item="gt.metaitem.01", subID=32711}, "oc:circuitChip1"]
  count: [1, 2]
  eu: 64
  time: 32
  output: 1
  inputFluid: {name="molten.solderingalloy", amount="72"}
}
chip3 {
  type: gt_assembler
  input: [{item="gt.metaitem.01", subID=32712}, "oc:circuitChip2"]
  count: [1, 2]
  eu: 256
  time: 32
  output: 1
  inputFluid: {name="molten.solderingalloy", amount="144"}
}

cable {
  type: gt_assembler
  input: [wireCopper, "string"]
  count: [4, 4]
  eu: 8
  time: 100
  output: 1
  inputFluid: {name="molten.rubber", amount="144"}
}

rawCircuitBoard {
  type: gt_cutter
  input: [stainedClayGreen]
  count: [1]
  eu: 16
  time: 200
  output: 4
  inputFluid: {name="lubricant", amount="1"}
}
disk {
  type: gt_formingPress
  input: [plateSteelMagnetic, ringSteelMagnetic]
  count: [1, 2]
  eu: 64
  time: 256
  output: 1
  inputFluid: {name="lubricant", amount="1"}
}
printedCircuitBoard {
  type: gt_formingPress
  input: ["oc:materialCircuitBoard", {item="gt.metaitem.01", subID=32716}]
  count: [1, 4]
  eu: 16
  time: 32
  output: 1
}
cu {
  input: [[wireGt01Gold, wireFinePlatinum, wireGt01Gold]
          ["oc:materialTransistor", circuitAdvanced, "oc:materialTransistor"]
          [wireGt01Gold, "oc:materialTransistor", wireGt01Gold]]
}
alu {
  input: [[wireGt01AnyCopper, wireFineGold, wireGt01AnyCopper]
          ["oc:materialTransistor", "oc:circuitChip1", "oc:materialTransistor"]
          [wireGt01AnyCopper, "oc:materialTransistor", wireGt01AnyCopper]]
}
cpu1 {
  input: [[wireGt01AnyCopper, wireFineGold, wireGt01AnyCopper]
          ["oc:circuitChip1", "oc:materialCU", "oc:circuitChip1"]
          [wireGt01AnyCopper, "oc:materialALU", wireGt01AnyCopper]]
}
cpu2 {
  input: [[wireGt01Gold, wireFineElectrum, wireGt01Gold]
          ["oc:circuitChip2", "oc:materialCU", "oc:circuitChip2"]
          [wireGt01Gold, "oc:materialALU", wireGt01Gold]]
}
cpu3 {
  input: [[wireGt01Aluminium, wireFinePlatinum, wireGt01Aluminium]
          ["oc:circuitChip3", "oc:materialCU", "oc:circuitChip3"]
          [wireGt01Aluminium, "oc:materialALU", wireGt01Aluminium]]
}
apu1 {
  input: [[wireFineElectrum, "oc:circuitChip1", wireFineElectrum]
          ["oc:cpu2", "oc:componentBus1", "oc:graphicsCard1"]
          [wireFineElectrum, "oc:circuitChip1", wireFineElectrum]]
}
apu2 {
  input: [[wireFinePlatinum, "oc:circuitChip2", wireFinePlatinum]
          ["oc:cpu3", "oc:componentBus2", "oc:graphicsCard2"]
          [wireFinePlatinum, "oc:circuitChip2", wireFinePlatinum]]
}
card {
  input: [[screwAnyIron, "oc:circuitChip1", "oc:materialTransistor"]
          [plateAnyIron, "oc:materialCircuitBoardPrinted", "oc:materialCircuitBoardPrinted"]
          [screwAnyIron, wireFineElectrum, wireFineElectrum]]
}
componentBus1 {
  input: [[wireGt01AnyCopper, wireFineGold, wireGt01AnyCopper]
          ["oc:circuitChip1", "oc:materialCU", ""]
          [wireGt01AnyCopper, "oc:materialCircuitBoardPrinted", wireGt01AnyCopper]]
}
componentBus2 {
  input: [[wireGt01Gold, wireFineElectrum, wireGt01Gold]
          ["oc:circuitChip2", "oc:materialCU", ""]
          [wireGt01Gold, "oc:materialCircuitBoardPrinted", wireGt01Gold]]
}
componentBus3 {
  input: [[wireGt01Aluminium, wireFinePlatinum, wireGt01Aluminium]
          ["oc:circuitChip3", "oc:materialCU", ""]
          [wireGt01Aluminium, "oc:materialCircuitBoardPrinted", wireGt01Aluminium]]
}
dataCard1 {
  input: [[wireFineGold, "oc:materialALU", "oc:circuitChip2"]
          ["", "oc:materialCard", ""]]
}
dataCard2 {
  input: [[wireFineElectrum, "oc:cpu1", "oc:circuitChip3"]
          ["", "oc:dataCard1", ""]]
}
dataCard3 {
  input: [[wireFinePlatinum, "oc:cpu2", "oc:ram5"]
          ["", "oc:dataCard2", ""]]
}
hdd1 {
  input: [["oc:circuitChip1", "oc:materialDisk", wireFineGold]
          ["oc:materialCircuitBoardPrinted", "oc:materialDisk", {item="gt.metaitem.01", subID=32601}]
          ["oc:circuitChip1", "oc:materialDisk", cableGt01AnyCopper]]
}
hdd2 {
  input: [["oc:circuitChip2", "oc:materialDisk", wireFineElectrum]
          ["oc:materialCircuitBoardPrinted", "oc:materialDisk", {item="gt.metaitem.01", subID=32602}]
          ["oc:circuitChip2", "oc:materialDisk", cableGt01Gold]]
}
hdd3 {
  input: [["oc:circuitChip3", "oc:materialDisk", wireFinePlatinum]
          ["oc:materialCircuitBoardPrinted", "oc:materialDisk", {item="gt.metaitem.01", subID=32603}]
          ["oc:circuitChip3", "oc:materialDisk", cableGt01Aluminium]]
}
droneCase1 {
  input: [[rotorCarbon, "oc:materialCU", rotorCarbon]
          [{item="gt.metaitem.01", subID=32601}, "oc:microcontrollerCase1", {item="gt.metaitem.01", subID=32601}]
          ["oc:circuitChip1", "oc:componentBus2", "oc:circuitChip1"]]
}
droneCase2 {
  input: [[rotorCarbon, "oc:materialCU", rotorCarbon]
          [{item="gt.metaitem.01", subID=32602}, "oc:microcontrollerCase2", {item="gt.metaitem.01", subID=32602}]
          ["oc:circuitChip2", "oc:componentBus3", "oc:circuitChip2"]]
}
floppy {
  input: [[sheetPlastic, {item="ic2.itemCasing", subID=5}, sheetPlastic]
          [paperEmpty, "oc:materialDisk", paperEmpty]
          [sheetPlastic, paperEmpty, sheetPlastic]]
}
inkCartridgeEmpty {
  input: [[sheetPlastic, {item="gt.metaitem.01", subID=32611}, sheetPlastic]
          ["oc:circuitChip1", cellEmpty, "oc:circuitChip1"]
          [sheetPlastic, "oc:materialCircuitBoardPrinted", sheetPlastic]]
}
microcontrollerCase1 {
  input: [[cableGt01AnyCopper, "oc:circuitChip1", cableGt01AnyCopper]
          [wireFineGold, {block="gt.blockmachines", subID=12}, wireFineGold]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
microcontrollerCase2 {
  input: [[cableGt01Aluminium, "oc:circuitChip2", cableGt01Aluminium]
          [wireFinePlatinum, {block="gt.blockmachines", subID=13}, wireFinePlatinum]
          [cableGt01Aluminium, "oc:materialCircuitBoardPrinted", cableGt01Aluminium]]
}
redstoneCard1 {
  input: [[wireGt01RedAlloy, "oc:circuitChip1", ""]
          ["", "oc:materialCard", ""]]
}
redstoneCard2 {
  input: [[wireGt08RedAlloy, "oc:circuitChip2", enderPearl]
          ["", "oc:redstoneCard1", ""]]
}
server1 {
  input: [["oc:circuitChip1", "oc:ram4", "oc:circuitChip1"]
          ["oc:circuitChip2", "oc:case1", "oc:circuitChip2"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
server2 {
  input: [["oc:circuitChip2", "oc:ram5", "oc:circuitChip2"]
          ["oc:circuitChip3", "oc:case2", "oc:circuitChip3"]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
server3 {
  input: [["oc:circuitChip3", "oc:ram6", "oc:circuitChip3"]
          ["oc:circuitChip3", "oc:case3", "oc:circuitChip3"]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
tabletCase1 {
  input: [[wireFinePlatinum, button, plateStainlessSteel]
          ["oc:componentBus1", "oc:screen2", "oc:circuitChip3"]
          [wireFinePlatinum, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
tabletCase2 {
  input: [["oc:circuitChip2", button, plateTitanium]
          ["oc:componentBus3", "oc:screen2", "oc:circuitChip3"]
          ["oc:circuitChip2", "oc:materialCircuitBoardPrinted", plateTitanium]]
}
terminal {
  input: [[plateStainlessSteel, button, plateStainlessSteel]
          ["oc:circuitChip3", "oc:screen2", "oc:wlanCard"]
          [plateStainlessSteel, "oc:keyboard", plateStainlessSteel]]
}
eeprom {
  input: [[sheetPlastic, wireFineGold, sheetPlastic]
          [circuitPrimitive, "oc:circuitChip1", circuitPrimitive]
          [sheetPlastic, paperEmpty, sheetPlastic]]
}
adapter {
  input: [["oc:circuitChip1", circuitGood, "oc:circuitChip1"]
          ["oc:cable", {block="gt.blockmachines", subID=12}, "oc:cable"]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
charger {
  input: [["oc:circuitChip2", circuitGood, "oc:circuitChip2"]
          ["oc:capacitor", {block="gt.blockmachines", subID=692}, "oc:capacitor"]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
diskDrive  {
  input: [["oc:circuitChip1", circuitGood, "oc:circuitChip1"]
          [{item="gt.metaitem.01", subID=32641}, {block="gt.blockmachines", subID=12}, {item="gt.metaitem.01", subID=32601}]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
geolyzer {
  input: [["oc:circuitChip2", {block="gt.blockmachines", subID=343}, "oc:circuitChip2"]
          [circuitElite, {block="gt.blockmachines", subID=13}, circuitElite]
          [cableGt01Gold, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
hologram1 {
  input: [["oc:circuitChip2", circuitAdvanced, "oc:circuitChip2"]
          [{item="gt.metaitem.01", subID=32682}, {block="gt.blockmachines", subID=13}, {item="gt.metaitem.01", subID=32682}]
          [cableGt01Gold, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
hologram2 {
  input: [["oc:circuitChip3", circuitElite, "oc:circuitChip3"]
          [{item="gt.metaitem.01", subID=32683}, {block="gt.blockmachines", subID=14}, {item="gt.metaitem.01", subID=32683}]
          [cableGt01Aluminium, "oc:materialCircuitBoardPrinted", cableGt01Aluminium]]
}
motionSensor {
  input: [["oc:circuitChip2", "oc:cpu2", "oc:circuitChip2"]
          [{item="gt.metaitem.01", subID=32692}, {block="gt.blockmachines", subID=13}, {item="gt.metaitem.01", subID=32692}]
          [cableGt01Gold, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
powerConverter {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [cableGt01AnyCopper, {block="gt.blockmachines", subID=22}, "oc:cable"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
powerDistributor {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          ["oc:cable", {block="gt.blockmachines", subID=12}, "oc:cable"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
raid {
  input: [[plateTitanium, "oc:cpu3", plateTitanium]
          ["oc:ram3", "oc:diskDrive", "oc:ram3"]
          [plateTitanium, "oc:circuitChip2", plateTitanium]]
}
redstone {
  input: [["oc:redstoneCard1", "oc:circuitChip3", "oc:redstoneCard1"]
          [wireGt08RedAlloy, {block="gt.blockmachines", subID=12}, wireGt08RedAlloy]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
relay {
  input: [["oc:lanCard", "oc:circuitChip2", "oc:lanCard"]
          ["oc:cable", {block="gt.blockmachines", subID=12}, "oc:cable"]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
screen1 {
  input: [["oc:circuitChip1", circuitGood, "oc:circuitChip1"]
          [{item="gt.metaitem.01", subID=32740}, {block="gt.blockmachines", subID=12}, "oc:cable"]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
screen2 {
  input: [["oc:circuitChip2", circuitAdvanced, "oc:circuitChip2"]
          [{item="gt.metaitem.01", subID=32740}, {block="gt.blockmachines", subID=13}, "oc:cable"]
          [cableGt01Gold, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
screen3 {
  input: [["oc:circuitChip3", circuitElite, "oc:circuitChip3"]
          [{item="gt.metaitem.01", subID=32740}, {block="gt.blockmachines", subID=14}, "oc:cable"]
          [cableGt01Aluminium, "oc:materialCircuitBoardPrinted", cableGt01Aluminium]]
}
serverRack {
  input: [["oc:circuitChip2", "oc:wlanCard", "oc:circuitChip2"]
          [{block="gregtech:gt.metaitem.02", subID=21306"}, {block="gt.blockmachines", subID=13}, {block="gregtech:gt.metaitem.02", subID=21306"}]
          ["oc:relay", "oc:materialCircuitBoardPrinted", "oc:powerDistributor"]]
}
printer {
  input: [["oc:circuitChip2", circuitAdvanced, "oc:circuitChip2"]
          [cableGt02Cupronickel, {block="gt.blockmachines", subID=322}, {item="gt.metaitem.01", subID=32642}]
          [{item="gt.metaitem.01", subID=32602}, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
waypoint {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          ["oc:materialTransistor", "oc:materialInterweb", "oc:materialTransistor"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
netSplitter {
  input: [[plateAluminium, "oc:cable", plateAluminium]
          ["oc:cable", {item="ic2.itemCable", subID=12}, "oc:cable"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
transposer {
  input: [[plateStainlessSteel, "oc:inventoryControllerUpgrade", plateStainlessSteel]
          [pipeMediumElectrum, {block="gt.blockmachines", subID=9232}, pipeMediumStainlessSteel]
          [plateStainlessSteel, "oc:tankControllerUpgrade", plateStainlessSteel]]
}

assembler { 
  input: [["oc:materialCU", craftingWorkBench, "oc:materialCU"]
          ["oc:circuitChip2", {block="gt.blockmachines", subID=212}, "oc:circuitChip2"]
          [cableGt01AnyCopper, circuitGood, cableGt01AnyCopper]]
}
disassembler { 
  input: [["oc:materialCU", "oc:analyzer", "oc:materialCU"]
          ["oc:circuitChip2", {block="gt.blockmachines", subID=452}, "oc:circuitChip2"]
          [cableGt01AnyCopper, circuitGood, cableGt01AnyCopper]]
}
analyzer {
  type: shapeless
  input: [{item="gt.metaitem.01", subID=32762}, "oc:circuitChip3"]
}
capacitor { 
  input: [[{item="gt.metaitem.01", subID=32518}, circuitGood, {item="gt.metaitem.01", subID=32518}]
          ["oc:materialTransistor", {block="gt.blockmachines", subID=162}, "oc:materialTransistor"]
          [{item="gt.metaitem.01", subID=32518}, cableGt01AnyCopper, {item="gt.metaitem.01", subID=32518}]]
}
case1 {
  input: [["oc:circuitChip1", circuitGood, "oc:circuitChip1"]
          [{block="gregtech:gt.metaitem.02", subID=21300"}, {block="gt.blockmachines", subID=12}, {block="gregtech:gt.metaitem.02", subID=21300"}]
          [cableGt01AnyCopper, "oc:materialCircuitBoardPrinted", cableGt01AnyCopper]]
}
case2 {
  input: [["oc:circuitChip2", circuitAdvanced, "oc:circuitChip2"]
          [{block="gregtech:gt.metaitem.02", subID=21305"}, {block="gt.blockmachines", subID=13}, {block="gregtech:gt.metaitem.02", subID=21305"}]
          [cableGt01Gold, "oc:materialCircuitBoardPrinted", cableGt01Gold]]
}
case3 {
  input: [["oc:circuitChip3", circuitElite, "oc:circuitChip3"]
          [{block="gregtech:gt.metaitem.02", subID=21306"}, {block="gt.blockmachines", subID=14}, {block="gregtech:gt.metaitem.02", subID=21306"}]
          [cableGt01Aluminium, "oc:materialCircuitBoardPrinted", cableGt01Aluminium]]
}
angelUpgrade {
  input: [[plateStainlessSteel, enderPearl, plateStainlessSteel]
          ["oc:circuitChip1", {item="gt.metaitem.01", subID=32642}, "oc:circuitChip1"]
          [plateStainlessSteel, enderPearl, plateStainlessSteel]]
}
batteryUpgrade1 {
  input: [[plateAluminium, cableGt01AnyCopper, plateAluminium]
          ["oc:circuitChip1", "oc:capacitor", "oc:circuitChip1"]
          [plateAluminium, cableGt01AnyCopper, plateAluminium]]
}
batteryUpgrade2 {
  input: [[plateStainlessSteel, "oc:capacitor", plateStainlessSteel]
          ["oc:circuitChip2", cableGt01Gold, "oc:circuitChip2"]
          [plateStainlessSteel, "oc:capacitor", plateStainlessSteel]]
}
batteryUpgrade3 {
  input: [[plateTitanium, "oc:capacitor", plateTitanium]
          ["oc:circuitChip3", "oc:capacitor", "oc:circuitChip3"]
          [cableGt01Aluminium, "oc:capacitor", cableGt01Aluminium]]
}
chunkloaderUpgrade {
  input: [["oc:circuitChip3", enderPearl, "oc:circuitChip3"]
          [{item="gt.metaitem.01", subID=32670}, {block="Railcraft:machine.alpha"}, {item="gt.metaitem.01", subID=32670}]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
cardContainer1 {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [{item="gt.metaitem.01", subID=32641}, chest, ""]
          [plateAluminium, "oc:materialCard", plateAluminium]]
}
cardContainer2 {
  input: [[plateStainlessSteel, "oc:circuitChip2", plateStainlessSteel]
          [{item="gt.metaitem.01", subID=32642}, chest, ""]
          [plateStainlessSteel, "oc:materialCard", plateStainlessSteel]]
}
cardContainer3 {
  input: [[plateTitanium, "oc:circuitChip3", plateTitanium]
          [{item="gt.metaitem.01", subID=32643}, chest, ""]
          [plateTitanium, "oc:materialCard", plateTitanium]]
}
upgradeContainer1 {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [{item="gt.metaitem.01", subID=32641}, chest, ""]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
upgradeContainer2 {
  input: [[plateStainlessSteel, "oc:circuitChip2", plateStainlessSteel]
          [{item="gt.metaitem.01", subID=32642}, chest, ""]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
upgradeContainer3 {
  input: [[plateTitanium, "oc:circuitChip3", plateTitanium]
          [{item="gt.metaitem.01", subID=32643}, chest, ""]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
craftingUpgrade {
  input: [["oc:circuitChip3", "", "oc:circuitChip3"]
          [{item="gt.metaitem.01", subID=32653}, {block="gt.blockmachines", subID=213}, {item="gt.metaitem.01", subID=32653}]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
databaseUpgrade1 {
  input: [[plateAluminium, "oc:analyzer", plateAluminium]
          ["oc:circuitChip1", "oc:hdd1", "oc:circuitChip1"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
databaseUpgrade2 {
  input: [[plateStainlessSteel, "oc:analyzer", plateStainlessSteel]
          ["oc:circuitChip2", "oc:hdd2", "oc:circuitChip2"]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
databaseUpgrade3 {
  input: [[plateTitanium, "oc:analyzer", plateTitanium]
          ["oc:circuitChip3", "oc:hdd3", "oc:circuitChip3"]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
experienceUpgrade {
  input: [[plateTitanium, "", plateTitanium]
          ["oc:circuitChip3", emerald, "oc:circuitChip3"]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
generatorUpgrade {
  input: [[plateStainlessSteel, "", plateStainlessSteel]
          ["oc:circuitChip2", {block="gt.blockmachines", subID=1111}, "oc:circuitChip2"]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
hoverUpgrade1 {
  input: [[rotorCarbon, "oc:circuitChip1", rotorCarbon]
          [{item="gt.metaitem.01", subID=32601}, plateAluminium, {item="gt.metaitem.01", subID=32601}]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
hoverUpgrade2 {
  input: [[rotorCarbon, "oc:circuitChip2", rotorCarbon]
          [{item="gt.metaitem.01", subID=32602}, plateStainlessSteel, {item="gt.metaitem.01", subID=32602}]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
inventoryUpgrade {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [{item="gt.metaitem.01", subID=32651}, chest, {item="gt.metaitem.01", subID=32631}]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
inventoryControllerUpgrade {
  input: [[plateStainlessSteel, "oc:analyzer", plateStainlessSteel]
          [{item="gt.metaitem.01", subID=32652}, "oc:circuitChip2", {item="gt.metaitem.01", subID=32632}]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
leashUpgrade {
  input: [[plateAluminium, "minecraft:lead", plateAluminium]
          ["minecraft:lead", "oc:materialCU", "minecraft:lead"]
          [plateAluminium, "minecraft:lead", plateAluminium]]
}
navigationUpgrade {
  input: [[plateStainlessSteel, compass, plateStainlessSteel]
          ["oc:circuitChip2", paperMap, "oc:circuitChip2"]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
pistonUpgrade {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [{item="gt.metaitem.01", subID=32641}, plateAluminium, {item="gt.metaitem.01", subID=32641}]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
signUpgrade {
  input: [[plateAluminium, dyeBlack, plateAluminium]
          ["oc:circuitChip1", stickWood, "oc:circuitChip1"]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
solarGeneratorUpgrade {
  input: [[glass, glass, glass]
          ["oc:circuitChip3", {item="gt.metaitem.01", subID=32750}, "oc:circuitChip3"]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
tankUpgrade {
  input: [[plateAluminium, "oc:circuitChip1", plateAluminium]
          [{item="gt.metaitem.01", subID=32611}, {item="gt.metaitem.01", subID=32405}, {item="gt.metaitem.01", subID=32611}]
          [plateAluminium, "oc:materialCircuitBoardPrinted", plateAluminium]]
}
tankControllerUpgrade {
  input: [[plateStainlessSteel, "oc:circuitChip2", plateStainlessSteel]
          [{item="gt.metaitem.01", subID=32749}, pipeMediumStainlessSteel, {item="gt.metaitem.01", subID=32612}]
          [plateStainlessSteel, "oc:materialCircuitBoardPrinted", plateStainlessSteel]]
}
tractorBeamUpgrade {
  input: [[plateTitanium, "oc:circuitChip3", plateTitanium]
          [{item="gt.metaitem.01", subID=32670}, "oc:capacitor", {item="gt.metaitem.01", subID=32670}]
          [plateTitanium, "oc:materialCircuitBoardPrinted", plateTitanium]]
}
